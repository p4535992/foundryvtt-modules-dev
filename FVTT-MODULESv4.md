
## [MUST HAVE] MODULES PART 1

- [EXTERNAL][About Time](https://gitlab.com/tposney/about-time ) - https://gitlab.com/tposney/about-time/raw/master/src/module.json
- [EXTERNAL][FxMaster](https://gitlab.com/mesfoliesludiques/foundryvtt-fxmaster )- https://gitlab.com/mesfoliesludiques/foundryvtt-fxmaster/-/raw/master/module.json
- [EXTERNAL][Calendar/Weather](https://github.com/DasSauerkraut/calendar-weather ) - https://raw.githubusercontent.com/DasSauerkraut/calendar-weather/master/package/module.json
- [MAP][Hide Cursor](https://gitlab.com/foundry-azzurite/cursor-hider/-/tree/master ) - https://gitlab.com/foundry-azzurite/cursor-hider/-/raw/master/src/module.json
- [MACRO/COMBAT][The Furnace](https://github.com/kakaroto/fvtt-module-furnace ) - https://raw.githubusercontent.com/kakaroto/fvtt-module-furnace/master/module.json
- [MAP][M.E.S.S. (Moerill’s Enhancing Super Suite)](https://github.com/Moerill/mess ) - https://raw.githubusercontent.com/Moerill/Mess/master/src/module.json
- [MACRO/COMBAT][Trigger Happy](https://github.com/kakaroto/fvtt-module-trigger-happy/ ) - https://raw.githubusercontent.com/kakaroto/fvtt-module-trigger-happy/master/module.json
- [JOURNAL/CHAT][Polyglot](https://github.com/kakaroto/fvtt-module-polyglot ) - https://raw.githubusercontent.com/kakaroto/fvtt-module-polyglot/master/module.json
- [TOKEN][VTTA - Iconizer](https://www.vttassets.com/assets/vtta-iconizer )
- [TOKEN][VTTA - Tokenizer](https://www.vttassets.com/assets/vtta-tokenizer )
- [COMBAT][BUGGED ??][VTTA - Party Overview](https://www.vttassets.com/assets/vtta-party )
- [EXTERNAL][TidyUI](https://github.com/sdenec/tidy-ui_game-settings ) - https://raw.githubusercontent.com/sdenec/tidy-ui_game-settings/master/module.json
- [ENTITY][Navigation Presets](https://github.com/earlSt1/vtt-navigation-presets ) - https://raw.githubusercontent.com/earlSt1/vtt-navigation-presets/master/module.json 
- [ENTITY][Compendium Folders](https://github.com/earlSt1/vtt-compendium-folders ) - https://raw.githubusercontent.com/earlSt1/vtt-compendium-folders/master/module.json
- [ENTITY][Permission Viewer](https://github.com/kakaroto/fvtt-module-permission-viewer ) - https://raw.githubusercontent.com/kakaroto/fvtt-module-permission-viewer/master/module.json
- [TOKEN][Token Action HUD](https://github.com/espositos/fvtt-tokenactionhud ) - https://raw.githubusercontent.com/espositos/fvtt-tokenactionhud/master/module.json
- [TOKEN][Token Action HUD Artwork](https://github.com/zeel01/TokenHUDArtButton ) - https://raw.githubusercontent.com/zeel01/TokenHUDArtButton/master/module.json
- [CHAT][Token Chat Link](https://github.com/espositos/fvtt-tokenchatlink ) - https://raw.githubusercontent.com/espositos/fvtt-tokenchatlink/master/module.json

## [MUST HAVE] MODULES PART 2

- [TOKEN][Mount Up](https://gitlab.com/brunhine/foundry-mountup ) - https://gitlab.com/brunhine/foundry-mountup/raw/master/mountup/module.json
- [TOKEN][Follow me](https://gitlab.com/brunhine/foundry-followme/ ) - https://gitlab.com/brunhine/foundry-followme/raw/master/followme/module.json
- [TOKEN][Token Patrol](https://github.com/JacobMcAuley/foundry-patrol ) - https://raw.githubusercontent.com/JacobMcAuley/foundry-patrol/master/module.json
- [MACRO][Commnunity Macros](https://github.com/foundry-vtt-community/macros ) - https://raw.githubusercontent.com/foundry-vtt-community/macros/master/module.json 
- [MACRO][Personal Macro by kekilla](https://github.com/Kekilla0/Personal-Macros ) - 
- [TABLE][Community Tables](https://github.com/foundry-vtt-community/tables ) - https://raw.githubusercontent.com/foundry-vtt-community/tables/master/module.json
- [TOKEN][Token Auras](https://bitbucket.org/Fyorl/token-auras ) - https://bitbucket.org/Fyorl/token-auras/raw/master/module.json
- [MACRO/MAP][Teleport](https://github.com/knassher/FVTT-Teleport ) - https://raw.githubusercontent.com/knassher/FVTT-Teleport/master/module.json
- [EXTERNAL][Theatre](https://gitlab.com/mesfoliesludiques/theatre ) - https://gitlab.com/mesfoliesludiques/theatre/raw/master/module.json
- [EXTERNAL][Forien Quest Log](https://github.com/Forien/foundryvtt-forien-quest-log ) - https://raw.githubusercontent.com/Forien/foundryvtt-forien-quest-log/master/module.json
- [EXTERNAL][FoundryVTT - Workshop's Party Unit Frames](https://github.com/Foundry-Workshop/party-unit-frames ) - https://raw.githubusercontent.com/Foundry-Workshop/party-unit-frames/master/src/module.json

- [WALL][Merge Wall](https://gitlab.com/tposney/mergewalls ) - https://gitlab.com/tposney/mergewalls/raw/master/module.json
- [WALL][Wall Height](https://github.com/schultzcole/FVTT-Wall-Height ) - https://raw.githubusercontent.com/schultzcole/FVTT-Wall-Height/master/module.json
- [WALL][Wall Cutter](https://github.com/HadaIonut/WallCutter ) - https://raw.githubusercontent.com/HadaIonut/WallCutter/master/src/module.json

- [LIGHT][Community Lighting for FVTT](https://github.com/BlitzKraig/fvtt-CommunityLighting ) - https://raw.githubusercontent.com/BlitzKraig/fvtt-CommunityLighting/master/module.json
- [LIGHT][Community Lighting for FVTT](https://github.com/BlitzKraig/fvtt-CommunityLighting ) - https://raw.githubusercontent.com/BlitzKraig/fvtt-CommunityLighting/master/module.json

## [MUST HAVE] MODULES PART 3

- [MAP/SCENE][Import DungeonDraft](https://github.com/moo-man/FVTT-DD-Import ) - https://raw.githubusercontent.com/moo-man/FVTT-DD-Import/master/module.json
- [ANIMATED TEMPLATE][Animated Spells](https://github.com/jackkerouac/animated-spell-effects ) - https://raw.githubusercontent.com/jackkerouac/animated-spell-effects/master/module.json
- [ANIMATED TEMPLATE][Jinker](https://www.patreon.com/jinker )
- [MAP][Multilevel Tokens module for Foundry VTT](https://github.com/grandseiken/foundryvtt-multilevel-tokens ) - https://raw.githubusercontent.com/grandseiken/foundryvtt-multilevel-tokens/master/module.json
- [TOKEN][Adjusted Movement](https://github.com/eadorin/adjusted-movement ) - https://raw.githubusercontent.com/eadorin/adjusted-movement/master/module.json
- [MAP][Parallaxia](https://gitlab.com/reichler/parallaxia ) - https://gitlab.com/reichler/parallaxia/raw/master/parallaxia/module.json
- [TOKEN/SCENES][Everybody Look](https://github.com/winks-vtt/everybody-look ) - https://raw.githubusercontent.com/winks-vtt/everybody-look/master/module.json

- [EXTERNAL][Adventure Import Export](https://github.com/cstadther/adventure-import-export ) - https://raw.githubusercontent.com/cstadther/adventure-import-export/master/module.json
- [MAP/FOG][Reset Doors and Fog](https://github.com/wsaunders1014/resetdoorsandfog ) - https://raw.githubusercontent.com/wsaunders1014/resetdoorsandfog/master/module.json 
- [MAP][Show Door Icons](https://github.com/wsaunders1014/ShowDoorIcons/ ) - https://raw.githubusercontent.com/wsaunders1014/ShowDoorIcons/master/module.json

- [MAP][Armreach](https://github.com/psyny/FoundryVTT/tree/master/ArmsReach ) - https://raw.githubusercontent.com/psyny/FoundryVTT/master/ArmsReach/arms-reach/module.json
- [LIGHT][Dynamic Illumination](https://github.com/delVhariant/illumination/ ) - https://raw.githubusercontent.com/delVhariant/illumination/master/module.json
- [JOURNAL][Library PDFoundry](https://github.com/Djphoenix719/PDFoundry ) - https://raw.githubusercontent.com/Djphoenix719/PDFoundry/master/module.json
- [AUDIO][Background Volume](https://github.com/mtvjr/background-volume ) - https://raw.githubusercontent.com/mtvjr/background-volume/master/module.json
- [TOKEN][About Face](https://foundryvtt.com/packages/about-face/ ) - https://raw.githubusercontent.com/eadorin/about-face/master/module.json

## [MUST HAVE] MODULES PART 4

- [TOKEN/MAP][Drag Upload](https://github.com/cswendrowski/FoundryVTT-Drag-Upload ) - https://raw.githubusercontent.com/cswendrowski/FoundryVTT-Drag-Upload/master/module.json
- [JOURNAL][Mindmap\Graphs](https://gitlab.com/moerills-fvtt-modules/graphs ) - https://gitlab.com/moerills-fvtt-modules/graphs/raw/master/src/module.json
- [TOKEN/ENTITY/CHARACTER SHEET][Pick-Up-Stix](https://github.com/kamcknig/pick-up-stix ) - https://raw.githubusercontent.com/kamcknig/pick-up-stix/develop/src/module.json
- [TOKEN][Token Attacher](https://github.com/KayelGee/token-attacher ) - https://raw.githubusercontent.com/KayelGee/token-attacher/master/module.json
- [AUDIO][Playlist Import](https://github.com/JacobMcAuley/playlist_import ) - https://raw.githubusercontent.com/JacobMcAuley/playlist_import/master/module.json

- [TOKEN/MACRO][Token Hotbar](https://github.com/janssen-io/foundry-token-hotbar ) - https://raw.githubusercontent.com/janssen-io/foundry-token-hotbar/master/module.json
- [TOKEN/MACRO][Custom Hotbar](https://github.com/Norc/foundry-custom-hotbar ) - https://raw.githubusercontent.com/Norc/foundry-custom-hotbar/master/module.json
- [MAP/ANIMATED TEMPLATE] - https://foundryvtt.com/packages/jaamod/
- [AUDIO][Music Assist](https://github.com/temportalflux/MusicAssist )

- [AUDIO][Music Assistant](https://github.com/temportalflux/MusicAssist ) - https://raw.githubusercontent.com/temportalflux/MusicAssist/master/module.json
- [MAP/FOG][Simple Fog](https://github.com/VanceCole/simplefog ) - https://raw.githubusercontent.com/VanceCole/simplefog/master/module.json


- [TOKEN][Token Owner Selection Tweak](https://github.com/ElfFriend-DnD/foundryvtt-tokenOwnerSelectionTweak ) - https://raw.githubusercontent.com/ElfFriend-DnD/foundryvtt-tokenOwnerSelectionTweak/master/src/module.json
- [AUDIO][Sound Board by Blitz](https://github.com/BlitzKraig/fvtt-SoundBoard ) - https://raw.githubusercontent.com/BlitzKraig/fvtt-SoundBoard/master/module.json
- [JOURNAL/TOKEN][Automatic journal Icon Numbers](https://gitlab.com/tiwato/journal_icon_numbers ) - https://gitlab.com/tiwato/journal_icon_numbers/raw/master/module.json
- [UTILITY/MODULES][DF Settings Clarity](https://github.com/flamewave000/dragonflagon-fvtt/tree/master/df-settings-clarity ) - https://raw.githubusercontent.com/flamewave000/dragonflagon-fvtt/master/df-settings-clarity/module.json

## [MUST HAVE] MODULES GENERIC MUST HAVE (Quality of Life Improvements For Everyone)

- [CHAT][Chat Images](https://github.com/bmarian/chat-images ) - https://raw.githubusercontent.com/bmarian/chat-images/master/src/module.json
- [TOKEN][Health Estimate](https://gitlab.com/tsuki.no.mai/healthestimate/ ) - https://gitlab.com/tsuki.no.mai/healthestimate/-/raw/master/src/module.json
- [ENTITY][Search Anywhere](https://gitlab.com/riccisi/foundryvtt-search-anywhere ) - https://gitlab.com/riccisi/foundryvtt-search-anywhere/raw/master/module/module.json
- [TOKEN][Show Drag Dinstance](https://github.com/wsaunders1014/ShowDragDistance ) - https://raw.githubusercontent.com/wsaunders1014/ShowDragDistance/master/module.json
- [JOURNAL][Journal Sync](https://github.com/sytone/foundry-vtt-journal-sync ) - https://raw.githubusercontent.com/sytone/foundry-vtt-journal-sync/master/module.json
- [CHAT][Actually Private Rolls](https://github.com/syl3r86/Actually-Private-Rolls) - https://raw.githubusercontent.com/syl3r86/Actually-Private-Rolls/master/module.json

- [ENTITY][Compendiumn Browse](https://github.com/syl3r86/compendium-browser ) - https://raw.githubusercontent.com/syl3r86/compendium-browser/master/module.json

- [TABLE][Better Rolltables](https://github.com/ultrakorne/better-rolltables ) - https://raw.githubusercontent.com/ultrakorne/better-rolltables/master/module.json
- [TEMPLATE][Token Magic FX](https://github.com/Feu-Secret/Tokenmagic/ ) - https://raw.githubusercontent.com/Feu-Secret/Tokenmagic/master/tokenmagic/module.json
- [ENTITY][Forien's Unidentified Items](https://github.com/Forien/foundryvtt-forien-unidentified-items ) - https://raw.githubusercontent.com/Forien/foundryvtt-forien-unidentified-items/master/module.json
- [TOKEN/ENTITY][Token Mold](https://github.com/Moerill/token-mold#token-mold ) - https://raw.githubusercontent.com/Moerill/token-mold/master/src/module.json
- [ENTITY][Item Collection ](https://gitlab.com/tposney/itemcollection ) - https://gitlab.com/tposney/itemcollection/raw/master/module.json
- [COMPENDIUM][Smip Compendium](https://gitlab.com/fohswe/smip-compendium ) - https://gitlab.com/fohswe/smip-compendium/raw/master/module.json
- [ENITITY][Magic Items](https://gitlab.com/riccisi/foundryvtt-magic-items ) - https://gitlab.com/riccisi/foundryvtt-magic-items/raw/master/module/module.json
- [EXTERNAL][Travel Pace](https://github.com/rinnocenti/travel-pace ) - https://raw.githubusercontent.com/rinnocenti/travel-pace/master/module.json
- [ENTITY][Item Macro](https://github.com/Kekilla0/Item-Macro ) - https://raw.githubusercontent.com/Kekilla0/Item-Macro/master/module.json
- [EXTERNAL][Speech to Chat](https://github.com/Exote/foundry-vtt-speech-to-chat/ ) - https://raw.githubusercontent.com/Exote/foundry-vtt-speech-to-chat/master/module.json
- [JOURNAL][Custom Journal](https://github.com/Sanyella/FVTT-Custom-Journal-Theming ) - https://raw.githubusercontent.com/Sanyella/FVTT-Custom-Journal-Theming/master/module.json
- [JOURNAL/ACTOR][SideBar Aesthetics](https://gitlab.com/Furyspark/foundryvtt-sidebar-aesthetics/-/tree/master ) - https://gitlab.com/Furyspark/foundryvtt-sidebar-aesthetics/raw/master/module.json
- [CHAT][Hide GM Rolls](https://github.com/sPOiDar/fvtt-module-hide-gm-rolls ) - https://raw.githubusercontent.com/sPOiDar/fvtt-module-hide-gm-rolls/master/module.json
- [CHARACTER SHEET/UTILITY][Inventory Plus](https://github.com/syl3r86/inventory-plus ) - https://raw.githubusercontent.com/syl3r86/inventory-plus/master/module.json
- [CHARACTER SHEET/UTILITY][Show Artwork](https://github.com/wsaunders1014/ShowArtwork/ ) - https://raw.githubusercontent.com/wsaunders1014/ShowArtwork/master/module.json
- [CHAT][Foundry VTT - Narrator Tools](https://github.com/elizeuangelo/fvtt-module-narrator-tools ) - https://raw.githubusercontent.com/elizeuangelo/fvtt-module-narrator-tools/master/module.json
- [JOURNAL][journal-links](https://github.com/Sigafoos/journal-links ) - https://raw.githubusercontent.com/Sigafoos/journal-links/master/module.json
- [ROLL][Dice Calculator/Dice Tray](https://gitlab.com/asacolips-projects/foundry-mods/foundry-vtt-dice-calculator/ ) - https://gitlab.com/asacolips-projects/foundry-mods/foundry-vtt-dice-calculator/raw/master/module.json
- [TABLE][1000 Fish](https://github.com/cswendrowski/FoundryVTT-1000-Fish ) - https://raw.githubusercontent.com/cswendrowski/FoundryVTT-1000-Fish/master/module.json
- [JOURNAL][Markdown Editor](https://github.com/Moerill/fvtt-markdown-editor) - https://raw.githubusercontent.com/Moerill/fvtt-markdown-editor/master/src/module.json
- [JOURNAL][sound-link](https://github.com/superseva/sound-link ) - https://raw.githubusercontent.com/superseva/sound-link/master/module.json
- [Utility][Zoom/Pan Options](https://github.com/itamarcu/ZoomPanOptions) - https://raw.githubusercontent.com/itamarcu/ZoomPanOptions/master/module.json
- [UTILITY][Alternative Rotation](https://github.com/itamarcu/AlternativeRotation ) - https://raw.githubusercontent.com/itamarcu/AlternativeRotation/master/module.json 
- [JOURNAL][Selective Show](https://github.com/moo-man/FVTT-SelectiveShow ) - https://raw.githubusercontent.com/moo-man/FVTT-SelectiveShow/master/module.json
- [JOURNAL][Manny-Language](https://github.com/MannyKSoSo/Manny-Language ) - https://raw.githubusercontent.com/MannyKSoSo/Manny-Language/main/module.json

## [MUST HAVE] MODULES D&D5th  MUST HAVE

- [EXTERNAL][Plutonium (5etools)](https://get.5e.tools/plutonium/module.json)
- [CHARACTER SHEET][Variant Encumbrance 5e](https://github.com/VanirDev/VariantEncumbrance )- https://raw.githubusercontent.com/VanirDev/VariantEncumbrance/master/dist/module.json
- [CHARACTER SHEET][FVTT Long Rest HD Healing 5e](https://github.com/schultzcole/FVTT-Long-Rest-HD-Healing-5e/ ) - https://raw.githubusercontent.com/schultzcole/FVTT-Long-Rest-HD-Healing-5e/master/module.json
- [COMBAT][Chat Damage Buttons (5e)](https://gitlab.com/hooking/foundry-vtt---chat-damage-buttons ) - https://gitlab.com/hooking/foundry-vtt---chat-damage-buttons/raw/master/module.json
- [ENTITY][Not Enough NPCs: A 5e NPC Generator](https://github.com/ardittristan/VTTNPCGen ) - https://raw.githubusercontent.com/ardittristan/VTTNPCGen/master/module.json
- [CHARACTER SHEET][5e-Sheet Resources Plus](https://github.com/ardittristan/5eSheet-resourcesPlus ) - https://raw.githubusercontent.com/ardittristan/5eSheet-resourcesPlus/master/module.json
- [COMPENDIUM][Summoner](https://github.com/Jonwh25/summoner ) - https://raw.githubusercontent.com/Jonwh25/summoner/master/module.json 
- [CHARACTER SHEET/UTILITY][FVTT-Skill-Customization-5e](https://github.com/schultzcole/FVTT-Skill-Customization-5e ) - https://raw.githubusercontent.com/schultzcole/FVTT-Skill-Customization-5e/master/module.json
- [CHARACTER SHEET/UTILITY][Loot Sheet NPC 5e](https://github.com/jopeek/fvtt-loot-sheet-npc-5e ) - https://raw.githubusercontent.com/jopeek/fvtt-loot-sheet-npc-5e/master/module.json
- [JOURNAL/COMPENDIUM][Forgotten Adventures DM Screen](https://gitlab.com/fohswe/fa-dm-screen ) - https://gitlab.com/fohswe/fa-dm-screen/raw/master/module.json
- [COMPENDIUM][Super Homebrew Pack](https://github.com/SuperNar3k/foundry ) - https://raw.githubusercontent.com/SuperNar3k/foundry/master/SupersHomebrewPack/module.json 
- [COMPENDIUM][Supplemental Bestiary Pack](https://github.com/sparkcity/fvtt-supplementalbestiary ) - https://raw.githubusercontent.com/sparkcity/fvtt-supplementalbestiary/master/module.json
- [CHARACTHER SHEET][Downtime Tracking 5e Training](https://github.com/crash1115/5e-training ) - https://raw.githubusercontent.com/crash1115/5e-training/master/module.json
- [CHARACTHER SHEET][Ethck's Downtime Tracking](https://github.com/Ethck/Ethck-s-Downtime-Tracking ) - https://raw.githubusercontent.com/Ethck/Ethck-s-Downtime-Tracking/master/module.json
- [COMPENDIUM][Kobold Press OGL](http://kpogl.wdfiles.com/local--files/home:home/module.json) 
- [COMPENDIUM][um5-ultramodern-ogl](https://github.com/MrBrentRogers/UM5-Ultramodern5-OGL ) - https://raw.githubusercontent.com/MrBrentRogers/UM5-Ultramodern5-OGL/master/module.json
- [ITEM/ACTIVE EFFECTES][Dynamic-Effects-SRD](https://github.com/kandashi/Dynamic-Effects-SRD ) - https://raw.githubusercontent.com/kandashi/Dynamic-Effects-SRD/master/module.json
- [COMPENDIUM/IMPORT][Critter DB Import](https://github.com/jwinget/fvtt-module-critterdb-import/ ) - https://raw.githubusercontent.com/jwinget/fvtt-module-critterdb-import/master/module.json
- [TOKEN][Vehicles and Mechanisms](https://github.com/grandseiken/foundryvtt-vehicles-and-mechanisms ) -
- [CHARACTHER SHEET][Lazy Money](https://github.com/DeVelox/lazymoney )
- [CHARACTER SHEET][Tool Proficiencies for D&D5e](https://github.com/adriangaro/tool-proficiencies-5e )


- [HOTBAR][Illandril's Hotbar Uses](https://github.com/illandril/FoundryVTT-hotbar-uses ) - https://raw.githubusercontent.com/illandril/FoundryVTT-hotbar-uses/master/module.json
- [MAP/TOKEN/CONDITION][Illandril's Token Hud Scale](https://foundryvtt.com/packages/illandril-token-hud-scale/ ) - 
- [CHAT][Illandril's Chat Enhancements](https://github.com/illandril/FoundryVTT-chat-enhancements ) - https://raw.githubusercontent.com/illandril/FoundryVTT-chat-enhancements/master/module.json
- [TOKEN][Illandril's Token Tooltips (5e)](https://github.com/illandril/FoundryVTT-token-tooltips ) - https://raw.githubusercontent.com/illandril/FoundryVTT-token-tooltips/master/module.json

- [SCENE/TELEPORT][Point of Interest Teleporter](https://github.com/zeel01/poi-teleport )
- [SCENE/TELEPORT][REMAIN FOR OLD VERSION][Teleport](https://github.com/knassher/FVTT-Teleport )
- [UTILITY][FVVT POINTER](https://github.com/Moerill/fvtt-pointer ) - https://raw.githubusercontent.com/Moerill/fvtt-pointer/master/module.json

## [MUST HAVE] COMBAT UTILITY MODULES


- [COMBAT][Midi Qol](https://gitlab.com/tposney/midi-qol ) - https://gitlab.com/tposney/midi-qol/raw/master/src/module.json
- [COMBAT][Combat Utility Belt](https://github.com/death-save/combat-utility-belt ) - https://raw.githubusercontent.com/death-save/combat-utility-belt/master/module.json
- [COMBAT][Combat Ready](https://github.com/smilligan93/combatready ) - https://raw.githubusercontent.com/smilligan93/combatready/master/module.json

- [COMBAT][Target Enhancements](https://github.com/eadorin/target-enhancements ) - https://raw.githubusercontent.com/eadorin/target-enhancements/master/module.json
- [COMBAT][Combat Enhancements](https://gitlab.com/asacolips-projects/foundry-mods/combat-enhancements/ ) - https://gitlab.com/asacolips-projects/foundry-mods/combat-enhancements/raw/master/module.json
- [COMBAT][FVTT-Combat-Tracker-Effects-Tooltips](https://github.com/schultzcole/FVTT-Combat-Tracker-Effects-Tooltips ) - https://raw.githubusercontent.com/schultzcole/FVTT-Combat-Tracker-Effects-Tooltips/master/module.json

- [COMBAT][Cozy Player](https://github.com/psyny/FoundryVTT/tree/master/CozyPlayer ) - https://raw.githubusercontent.com/psyny/FoundryVTT/master/CozyPlayer/cozy-player/module.json
- [COMBAT][Condition Automation/Conditons](https://github.com/kandashi/condition-automation ) - https://raw.githubusercontent.com/kandashi/condition-automation/master/module.json
- [COMBAT][Turn Maker](https://gitlab.com/brunhine/foundry-turnmarker ) - https://gitlab.com/brunhine/foundry-turnmarker/-/raw/master/turnmarker/module.json
- [COMBAT][Turn Alert](https://github.com/schultzcole/FVTT-Turn-Alert/ ) - https://raw.githubusercontent.com/schultzcole/FVTT-Turn-Alert/master/module.json
- [COMBAT][Easy Target](https://bitbucket.org/Fyorl/easy-target/src/master/ ) - https://bitbucket.org/Fyorl/easy-target/raw/master/module.json
- [COMBAT/ROLL][Let me roll that for you](https://github.com/kakaroto/fvtt-module-lmrtfy ) - https://raw.githubusercontent.com/kakaroto/fvtt-module-lmrtfy/master/module.json
- [COMBAT][Foundry VTT Hidden Initiative](https://github.com/sfuqua/fvtt-hidden-initiative ) - https://raw.githubusercontent.com/sfuqua/fvtt-hidden-initiative/master/module.json
- [COMBAT][spellTemplateManager](https://github.com/bitkiller0/spellTemplateManager ) - https://raw.githubusercontent.com/bitkiller0/spellTemplateManager/master/module.json
- [COMBAT][ENTITY](https://github.com/Foundry-Workshop/ammo-swapper ) - https://raw.githubusercontent.com/Foundry-Workshop/ammo-swapper/master/src/module.json
- [COMBAT][ACTOR](https://github.com/CDeenen/NotYourTurn ) - https://raw.githubusercontent.com/CDeenen/NotYourTurn/master/module.json
- [COMBAT][UTILITY][Quick Encounters](https://github.com/opus1217/quick-encounters ) - https://raw.githubusercontent.com/opus1217/quick-encounters/master/module.json
- [COMBAT][ITEM][Better Curses](https://github.com/Wigsinator/betterCurses ) - 
- [COMBAT][I can see you](https://github.com/herasrobert/icu5e ) - https://raw.githubusercontent.com/herasrobert/icu5e/master/module.json

- [COMBAT][conditional visibility](https://github.com/gludington/conditional-visibility ) - https://raw.githubusercontent.com/gludington/conditional-visibility/master/src/module.json
- [COMBAT][Combat Carousel](https://github.com/death-save/combat-carousel-public ) - https://raw.githubusercontent.com/death-save/combat-carousel-public/main/module.json
- [COMBAT/TOKEN][Custom Nameplates](https://github.com/earlSt1/vtt-custom-nameplates ) - https://raw.githubusercontent.com/earlSt1/vtt-custom-nameplates/master/module.json
- [COMBAT/TOKEN][Icon Setter](https://github.com/Sky-Captain-13/foundry/tree/master/icon-setter )
- [COMBAT][DnD5e Helpers](https://github.com/trioderegion/dnd5e-helpers )
- [COMBAT/ACTIVE EFFECTS][Time Up](https://gitlab.com/tposney/times-up ) - 
- [COMABT][health monitor](https://github.com/rockshow888/health-monitor ) - https://raw.githubusercontent.com/rockshow888/health-monitor/master/module.json
- [COMBAT][mookAI](https://github.com/dwonderley/mookAI/ )
- [COMBAT][Blood and guts](https://github.com/edzillion/blood-n-guts )
- [COMBAT][IS BETTER MIDI QOL][Better Rolls for 5e](https://github.com/RedReign/FoundryVTT-BetterRolls5e ) - https://raw.githubusercontent.com/RedReign/FoundryVTT-BetterRolls5e/master/betterrolls5e/module.json 


## [MUST HAVE] MODULES COMPENDIUM

## [MUST HAVE] MODULES MAPS

- [MAP/COMPENDIUM][Loot Tavern magic Item](https://www.patreon.com/LootTavern )
- [MAP/COMPENDIUM][Tom Cartos Ostenwold] -
- [MAP/COMPENDIUM][Spellarena Battlemaps Collection]-
- [MAP][Battlemaps Domille](https://foundryvtt.s3-us-west-2.amazonaws.com/modules/dww-battlemaps/module.json )
- [MAP] https://foundryvtt.com/packages/mikwewa-free/
- [MAP] https://www.patreon.com/miskasmaps
- [MAP][WFRP4E - Fan-made Maps for Enemy in Shadows](https://gitlab.com/LeRatierBretonnien/wfrp4e-eis-maps) - https://gitlab.com/LeRatierBretonnien/wfrp4e-eis-maps/raw/master/module.json 
- [MAP][JB2A - Jules&Ben's Animated Assets](https://github.com/Jules-Bens-Aa/JB2A_DnD5e ) - https://raw.githubusercontent.com/Jules-Bens-Aa/JB2A_DnD5e/main/module.json
- [MAP][Inline Webviewer](https://github.com/ardittristan/VTTInlineWebviewer )
- [MAP/WORLD][The Cycle of Cerberus](https://www.patreon.com/posts/39846150 )
- [MAP/WORLD][The Turtle Guild](https://turtleguild-giftoftheweb.s3.us-east-2.amazonaws.com/world.json ) - https://topoturtle.com/products/gift-of-the-web-foundry-edition

# ======================================================================
# NOT ESSENTIAL JUST A REMAINDER OF THE WORK FROM THER WONDERUL DEVEOPER
# ======================================================================

## [MAJOR MODULES] INTERESTING BUT NOT ESSENTIAL

- [EXTERNAL][NOT ESSENTIAL][Butler](https://gitlab.com/anathemamask/butler-fvtt/ ) - https://gitlab.com/anathemamask/butler-fvtt/raw/master/module.json
- [ROLL/COMBAT]][Crash's Hide Skill Rolls](https://github.com/crash1115/blind-roll-skills )
- [MENU SETUP][Dragon Flagon Mods for Foundry VTT](https://github.com/flamewave000/dragonflagon-fvtt )
- [TOKEN][Ruler-from-Token](https://github.com/Nordiii/rulerfromtoken ) -
- [CHAT][Autocomplete Whisper](https://github.com/orcnog/autocomplete-whisper )
- [COMBAT][IS BETTER MIDI][Minor Qol](https://gitlab.com/tposney/minor-qol ) - https://gitlab.com/tposney/minor-qol/raw/master/module.json
- [JOURNAL] https://github.com/pumbers/journal-templates
- [TOKEN] - [Player Token Permissions Extended](https://github.com/VanceCole/player-token-permissions )
- [CHARACTR SHEET][NOT ESSENTIAL][(5e) Different Prepared and Equipped Colors](https://github.com/sparkcity/foundryvtt/tree/master/5edifferentpreparedandequippedcolors ) - https://raw.githubusercontent.com/sparkcity/foundryvtt/master/5edifferentpreparedandequippedcolors/module.json 
- [CHAT][Tabbed Chatlog][NOT ESSENTIAL](https://github.com/cswendrowski/FoundryVTT-Tabbed-Chatlog ) - https://raw.githubusercontent.com/cswendrowski/FoundryVTT-Tabbed-Chatlog/master/module.json
- [MAP][Less Fog][NOT ESSENTIAL][(https://github.com/trdischat/lessfog ) - https://raw.githubusercontent.com/trdischat/lessfog/master/module.json
- [LIGHT][NOT ESSENTIAL][Day Night Lights](https://github.com/EndlesNights/daynightlights ) - https://raw.githubusercontent.com/EndlesNights/daynightlights/master/daynightlights/module.json
- [TOKEN/LIGHT][NOT ESSENTIAL][Point of Vision](https://github.com/eadorin/point-of-vision ) - https://raw.githubusercontent.com/eadorin/point-of-vision/master/module.json
- [EXTERNAL][NOT ESSENTIAL][Popout](https://github.com/kakaroto/fvtt-module-popout ) - https://raw.githubusercontent.com/kakaroto/fvtt-module-popout/master/module.json
- [EXTERNAL][NOT ESSENTIAL][Popout Resizer](https://gitlab.com/cardagon/popout-resizer/ ) - https://gitlab.com/cardagon/popout-resizer/raw/master/module.json

- [ICON/MARKER][NOT ESSENTIAL][Lancer Condition Icons](https://github.com/Eranziel/lancer-conditions ) - https://raw.githubusercontent.com/Eranziel/lancer-conditions/master/module.json
- [CHAT][ON DEVELOPING][Monologue](https://github.com/delVhariant/monologue )
- [ENTITY][NEED UPDATE BUG WITH CUB][Quick Insert](https://gitlab.com/fvtt-modules-lab/quick-insert ) - https://gitlab.com/fvtt-modules-lab/quick-insert/-/jobs/artifacts/master/raw/module.json?job=build-module
- [MAP][GOOD BUT NOT ESSENSIAL][Control concealer](https://github.com/KayelGee/control-concealer ) - https://raw.githubusercontent.com/KayelGee/control-concealer/master/module.json
- [JOURNAL][GOOD BUT NOT ESSENSIAL][pdftofoundry](https://gitlab.com/fryguy1013/pdftofoundry ) - https://gitlab.com/fryguy1013/pdftofoundry/-/jobs/artifacts/master/raw/static_module/module.json?job=build
- [MAP][GOOD BUT NOT ESSENSIAL][Token HUD Wildcard for Foundry VTT](https://github.com/javieros105/FVTT-TokenHUDWildcard ) - https://raw.githubusercontent.com/javieros105/FVTT-TokenHUDWildcard/master/token-hud-wildcard/module.json
- [MAP][GOOD BUT NOT ESSENSIAL][Select tool everywhere](https://github.com/KayelGee/select-tool-everywhere ) https://raw.githubusercontent.com/KayelGee/select-tool-everywhere/master/module.json

- [MAP][GOOD BUT NOT ESSENSIAL][LetterTokens](https://github.com/buttonpushertv/lettertokens ) - https://raw.githubusercontent.com/Nevenall/foundry-escape-window/master/module.json
- [MAP][GOOD BUT NOT ESSENSIAL][EC MAp Remake](https://gitlab.com/Ustin/ec-maps-remake ) - https://gitlab.com/Ustin/ec-maps-remake/raw/master/module.json

- [TOKEN/COMBAT][GOOD BUT IS BETTER THE SCRIPT FROM MACRO COMMUNITY][Torch](https://github.com/RealDeuce/torch/ ) -  https://raw.githubusercontent.com/RealDeuce/torch/master/module.json
- [JOURNAL][GOOD BUT NOT ESSENSIAL][Pin Cushion](https://github.com/death-save/pin-cushion ) - https://raw.githubusercontent.com/death-save/pin-cushion/master/module.json
- [AUDIO][IS BETTER Playlist Import][Audio FOlder](https://github.com/megastruktur/foundryvtt-audio-folder ) - https://raw.githubusercontent.com/megastruktur/foundryvtt-audio-folder/master/module.json
- [SCENE][GOOD BUT NOT ESSENSIAL][scene-transitions](https://github.com/wsaunders1014/scene-transitions ) - https://raw.githubusercontent.com/wsaunders1014/scene-transitions/master/module.json
- [MAP/GRID][GOOD BUT NOT ESSENSIAL][Scale grid](https://github.com/UberV/scaleGrid ) - https://raw.githubusercontent.com/UberV/scaleGrid/master/module.json
- [MAP][Terrain Layer](https://github.com/wsaunders1014/TerrainLayer )


- [NOT ESSENTIAL][TEMPLATES][Better Templates](https://github.com/VanceCole/bettertemplates ) - https://raw.githubusercontent.com/VanceCole/bettertemplates/master/module.json
- [NOT ESSENTIAL][ENTITY][5E Encounter Builder](https://github.com/RaySSharma/fvtt-encounter-builder ) - https://raw.githubusercontent.com/RaySSharma/fvtt-encounter-builder/master/encounter-builder/module.json
- [NOT ESSENTIAL][CHARACTER SHEET][luckpoints](https://github.com/jakvike/luckpoints ) - https://raw.githubusercontent.com/jakvike/luckpoints/master/module.json
- [DEPRECATED][COMBAT][Combat Effects Tracker](https://github.com/MKamysz/combat-effects-tracker )- https://raw.githubusercontent.com/MKamysz/combat-effects-tracker/master/module.json
- [NOT ESSENTIAL][JOURNAL/SCENE][Forien's Scene Navigator](https://github.com/Forien/foundryvtt-forien-scene-navigator ) - https://raw.githubusercontent.com/Forien/foundryvtt-forien-scene-navigator/master/module.json
- [NOT ESSENTIAL][TOKEN][Token Audio](https://github.com/JacobMcAuley/token-audio ) - https://raw.githubusercontent.com/JacobMcAuley/token-audio/master/module.json

- [NOT ESSENTIAL][WALL][Wallter](https://github.com/mtvjr/wallter ) - https://raw.githubusercontent.com/mtvjr/wallter/master/module.json
- [NOT ESSENTIAL][WALL][Multiple Wall Points Mover](https://github.com/Reaver01/mwpm ) - https://raw.githubusercontent.com/Reaver01/mwpm/master/module.json
- [NOT ESSENTIAL][TOKEN][Tiny Tokens](https://github.com/BlitzKraig/fvtt-TinyTokens ) - https://github.com/BlitzKraig/fvtt-TinyTokens/blob/master/module.json
- [NOT ESSENTIAL][COMPENDIUM][redscompendia5e](https://github.com/RedReign/redscompendia5e ) - https://raw.githubusercontent.com/RedReign/redscompendia5e/master/redscompendia5e/module.json
- [NOT ESSENTIAL][Utility][Actor Sheet Macro](https://bitbucket.org/Fyorl/sheet-macro/src/master/ )
- [INTEGRATED IN THE CORE FOR 0.7.5][MAP/TOKEN][Token Drag Visibility](https://github.com/SteffanPoulsen/token-drag-visibility ) - https://raw.githubusercontent.com/SteffanPoulsen/token-drag-visibility/master/src/module.json
- [NOT ESSENTIAL][COMBAT][Combat Template Cleanup](https://github.com/Kekilla0/combat-template-cleanup ) - https://raw.githubusercontent.com/Kekilla0/combat-template-cleanup/master/module.json


## [MINOR MODULES] NOT ESSENTIAL 1

- [JOURNAL][NOT ESSENTIAL] https://github.com/moo-man/FVTT-SelectiveShow
- [TOKEN][NOT ESSENTIAL][ConditionIconHoverZoom](https://github.com/Sparkasaurusmex/ConditionIconHoverZoom ) - https://raw.githubusercontent.com/Sparkasaurusmex/ConditionIconHoverZoom/master/module.json 



- [DEV][NOT ESSENTIAL][libwrapper](https://github.com/ruipin/fvtt-lib-wrapper ) - https://raw.githubusercontent.com/ruipin/fvtt-lib-wrapper/master/module.json
- [TOKEN][NOT ESSENTIAL][Token Animation Tools](https://github.com/ruipin/fvtt-token-animation-tools ) - https://raw.githubusercontent.com/ruipin/fvtt-token-animation-tools/master/module.json
- [TOKEN/NAVBAR][NOT ESSENTIAL][FVTT Navbar Tweaks](https://github.com/ruipin/fvtt-navbar-tweaks ) - https://raw.githubusercontent.com/ruipin/fvtt-navbar-tweaks/master/module.json
- [LIGHT][NOT ESSENTIAL][FVTT Token Vision Tweaks](https://github.com/ruipin/fvtt-token-vision-tweaks ) - https://raw.githubusercontent.com/ruipin/fvtt-token-vision-tweaks/master/module.json

- [ICONS/MARKERS][NOT ESSENTIAL][Game Icons](https://github.com/datdamnzotz/icons ) - https://raw.githubusercontent.com/datdamnzotz/icons/master/module.json
- [CHAT][NOT ESSENTIAL CONFLICT WITH MIDI QOL][Chat Merge](https://github.com/FoundryModule/Modules/tree/master/ChatMerge ) - https://raw.githubusercontent.com/FoundryModule/Modules/master/ChatMerge/module.json

- ~[AUDIO][Ambient Doors](https://github.com/EndlesNights/ambientdoors ) - https://raw.githubusercontent.com/EndlesNights/ambientdoors/master/ambientdoors/module.json ~
- ~[AUDIO][Maestro](https://github.com/death-save/maestro ) - https://raw.githubusercontent.com/death-save/maestro/master/module.json ~


## [MINOR MODULES] NOT ESSENTIAL 2

- [ENTITY][NOT ESSENTIAL][Macro Folders](https://github.com/earlSt1/vtt-macro-folders ) - https://raw.githubusercontent.com/earlSt1/vtt-macro-folders/master/module.json
- [EXTERNAL][SUPPORT FOR CALENDAR][Dodecaluna ]() - https://raw.githubusercontent.com/Sparkasaurusmex/dodecaluna/master/module.json
- [MAP/ROLL][NOT ESSENTIAL][Dice So Nice](https://gitlab.com/riccisi/foundryvtt-dice-so-nice ) - https://gitlab.com/riccisi/foundryvtt-dice-so-nice/raw/master/module/module.json 
- [ROLL/CHAT][NOT ESSENTIAL][Chat Alias](https://github.com/nwen-dicekittens/Chat-Alias )
- [CHAT][FoundryVTT NPC Chatter](https://github.com/cswendrowski/FoundryVtt-Npc-Chatter ) - https://raw.githubusercontent.com/cswendrowski/FoundryVtt-Npc-Chatter/master/module.json
- [COMPENDIUM][NOT ESSENTIAL][IS BETTER PLUTONIUM][5eFeats] - https://github.com/jinkergm/5eFeats - https://raw.githubusercontent.com/jinkergm/5eFeats/master/module.json
- [NOT ESSNTIAL][whisper-dialog](https://github.com/Kekilla0/whisper-dialog) - https://raw.githubusercontent.com/Kekilla0/whisper-dialog/master/module.json
- [EXTERNAL] vtta-didyouknow - 
- [INTEGRATED IN CORE][Darker Vision for 5e](https://github.com/Voldemalort/darker-vision-for-5e ) - https://raw.githubusercontent.com/Voldemalort/darker-vision-for-5e/master/module.json
- [COMBAT][NOT ESSENTIAL][Bullseye](https://gitlab.com/Ionshard/foundry-vtt-bullseye ) - https://gitlab.com/Ionshard/foundry-vtt-bullseye/raw/master/module.json
- [MAP/COMBAT][IS BETTER Token Action HUD][Token Bar](https://github.com/Kekilla0/TokenBar/ )
- [AUDIO][Audio Folder](https://github.com/megastruktur/foundryvtt-audio-folder ) - https://raw.githubusercontent.com/megastruktur/foundryvtt-audio-folder/master/module.json
- [AUDIO][Better Ambient Loop](https://github.com/BlitzKraig/fvtt-BetterAmbientLoop ) 
- [TABLE][GOOD BUT NOT ESSENSIAL][RollTable Buttons](https://github.com/RaySSharma/fvtt-rolltable-buttons ) - https://raw.githubusercontent.com/RaySSharma/fvtt-rolltable-buttons/master/rolltable-buttons/module.json
- [MAP][GOOD BUT NOT ESSENSIAL][Minimap](https://gitlab.com/jesusafier/minimap ) - https://gitlab.com/jesusafier/minimap/-/jobs/artifacts/master/raw/module.json?job=build-module 
- [STYLE][TO MANY BUGS][Luminous](https://github.com/Sky-Captain-13/foundry/tree/master/luminous ) - https://raw.githubusercontent.com/Sky-Captain-13/foundry/master/luminous/luminous/module.json
- [TOKEN/ROLL][IS BETTER LMRTFY][Group Roll](https://github.com/trdischat/grouproll ) - https://raw.githubusercontent.com/trdischat/grouproll/master/module.json
- [CHAT][IS BETTER MINOR QUOL][Chat Portrait](https://github.com/ShoyuVanilla/FoundryVTT-Chat-Portrait/ ) - https://raw.githubusercontent.com/ShoyuVanilla/FoundryVTT-Chat-Portrait/master/module.json
- [TOKEN/COMPENDIUM][IS BETTER CUB][Conditions 5e](https://github.com/trdischat/conditions5e ) - https://raw.githubusercontent.com/trdischat/conditions5e/master/module.json
- [TOKEN][NEED UPDATE][Forien's Token Rotation](https://github.com/Forien/foundryvtt-forien-token-rotation ) - https://raw.githubusercontent.com/Forien/foundryvtt-forien-token-rotation/master/module.json
- [JOURNAL][NEED UPDATE][Show Notes](https://github.com/shawndibble/foundryvtt-show-notes )
- [COMBAT][IS BETTER BETTER ROLL][Foundry Token Health](https://github.com/tonifisler/foundry-token-health )
- [EXTERNAL][GOOD BUT NOT ESSENSIAL][Foundry Escape Window](https://github.com/Nevenall/foundry-escape-window )

- [CHAT][Better Text Drawing](https://foundryvtt.com/packages/better-text-drawings/ )
- [MAP][INTEGRATED IN CORE ??][Pings](https://gitlab.com/foundry-azzurite/pings/-/tree/master ) - https://gitlab.com/foundry-azzurite/pings/raw/master/src/module.json
- [EXTERNAL][Pin](https://bitbucket.org/Fyorl/foundry-pin/src/master/ ) - https://bitbucket.org/Fyorl/foundry-pin/raw/459f53c16164081142b43c8d3a8f65c78480a79e/module.json
- [SCENE][INTEGRATED IN CORE][Quick Scene View](https://gitlab.com/reichler/quicksceneview/ ) - https://gitlab.com/reichler/quicksceneview/raw/master/quicksceneview/module.json
- [CHAT][DEPRECATED][FoundryVTT-CGMP](https://github.com/ShoyuVanilla/FoundryVTT-CGMP) - https://raw.githubusercontent.com/ShoyuVanilla/FoundryVTT-CGMP/master/module/module.json

- [TABLES][EasyTable](https://github.com/BlitzKraig/fvtt-EasyTable ) - https://raw.githubusercontent.com/BlitzKraig/fvtt-EasyTable/master/module.json
- [EXTERNAL][World Anvil](https://gitlab.com/foundrynet/world-anvil )
- [CHAT][Whisper Box](https://github.com/Sk1mble/WhisperBox )
- [IMAGE][Compendium Image Mapper](https://gitlab.com/Wilco7302/compendium-image-mapper )
- [COMBAT][Health Monitor](https://github.com/rockshow888/health-monitor )
- [MAP][NEED UPDATE][Dr. Mapzo free maps (unofficial)](https://foundryvtt.com/packages/foundry-drmapzo-free/ )
- [JOURNAL][NOT ESSENTIAL][One Journal](https://gitlab.com/fvtt-modules-lab/one-journal ) - https://gitlab.com/fvtt-modules-lab/one-journal/raw/master/src/module.json
- [SCENE][DF Scene Enhancement](https://github.com/flamewave000/dragonflagon-fvtt/tree/master/df-scene-enhance )
- [IMPORTER][Tetra-Cube Importer](https://github.com/HadaIonut/Foundry-Markdown-Importer ) - https://raw.githubusercontent.com/HadaIonut/Foundry-Markdown-Importer/master/package.json
- [THEME][Whetstone Theme Manager](https://github.com/MajorVictory/Whetstone ) -
- [CHAT][Salon](https://gitlab.com/anathemamask/salon )

## [MINOR MODULES] NEED UPDATE OR DEPRECATED OR BE TESTED

- [DEPRECTAED 0.7.X(for 0.7.x use Dynamic Active Effects)][ENTITY][Dynamic Effects](https://gitlab.com/tposney/dynamiceffects ) - https://gitlab.com/tposney/dynamiceffects/raw/master/src/module.json
- [DEPRECATED][ENTITY][IS BETTER DYNAMIC EFFECTS][Effectifier](https://gitlab.com/ohporter/effectifier ) - https://gitlab.com/ohporter/effectifier/raw/master/module.json
- [DEPRECATED 0.7.X (Core feature, there is a new module, see https://www.reddit.com/r/FoundryVTT/comments/jgrpxq/075_community_lighting_announcement/)][MAP][Dancing Lights](https://github.com/BlitzKraig/fvtt-DancingLights ) - https://raw.githubusercontent.com/BlitzKraig/fvtt-DancingLights/master/module.json
- [DEPRECATED 0.7.X (Core feature, thanks /u/DrOctagon_MD)][ENTITY][Batch Permissions Folder](https://github.com/wsaunders1014/BatchPermissionsByFolder )
- [DEPRECATED][Cursor Zoom](https://github.com/itamarcu/CursorZoom/ ) - https://raw.githubusercontent.com/itamarcu/CursorZoom/master/module.json
- [DEPRECATED (Core feature, as a configuration setting which is off by default.)Dynamic Effects][MAP][Deselection](https://github.com/Sky-Captain-13/foundry/tree/master/deselection ) - https://raw.githubusercontent.com/Sky-Captain-13/foundry/master/deselection/deselection/module.json
- [TILE/CARD][FVTT Card Support](https://github.com/Brownie79/fvtt-card-support )
- [ROLL/ENTITY/TOKEN][Input Expression](https://github.com/zeel01/input-expressions ) - https://raw.githubusercontent.com/zeel01/input-expressions/master/module.json
- [COMPENDIUM][NOT ESSENTIAL][SRD Rules WIP](https://gitlab.com/fohswe/srd-rules-wip ) - https://gitlab.com/p4535992/srd-rules-wipsidebar/raw/master/module/raw/master/module.json
- [MAP][NEED UPDATE][Hex assists](https://github.com/Reaver01/Hex-Assist )
- ~[IS BETTER USE COZY PLAYER][Token Info Icons](https://github.com/jopeek/fvtt-token-info-icons )~
- [NEED UPDATE][Item Delete Check](https://gitlab.com/tposney/item-delete-check )

- [DEPRECATED] https://github.com/spacemandev-git/fvtt-cardimporter

- [NOT ESSENTIAL] https://gitlab.com/tenuki.igo/foundryvtt-ping-times
- [NOT ESSENTIAL] https://github.com/sean8223/coloredeffects
- [NOT ESSENTIAL] https://github.com/PaulEndri/fvtt-compendium-importer
- [NOT ESSENTIAL] https://github.com/VincentBlackhorse/tile-mask
- [NOT ESSENTIAL] https://github.com/ardittristan/VTTReturnToSetup
- [NOT ESSENTIAL] https://github.com/ardittristan/VTTCharSpreadSheet
- [NOT ESSENTIAL] https://github.com/Sk1mble/XCard
- [NOT ESSENTIAL] https://github.com/nwen-dicekittens/Relative-Tile-Size
- [NOT ESSENTIAL] https://github.com/syl3r86/statdrawer
- [NOT ESSENTIAL] https://github.com/FoundryModule/Modules/releases/download/0.2.0/module.json
- [NOT ESSENTIAL] https://raw.githubusercontent.com/KayelGee/DrawingTokenizer/v1.0.1/module.json
- [NOT ESSENTIAL] https://raw.githubusercontent.com/bmarian/sadness-chan/f-0.7.x/src/module.json
- [NOT ESSENTIAL] https://github.com/aka-beer-buddy/fvtt-cycle-token-stack
- [NOT ESSENTIAL] https://github.com/Forien/foundryvtt-forien-easy-polls
- [NOT ESSENTIAL] https://foundryvtt.com/packages/sadness-chan/
- [NOT ESSENTIAL] https://github.com/Xacus/demonlord
- [NOT ESSENTIAL] https://foundryvtt.com/packages/custom-css/
- [NOT ESSENTIAL] https://github.com/ardittristan/VttPersonalMarkers
- [NOT ESSENTIAL] https://foundryvtt.com/packages/fvtt-cardimporter/
- [NOT ESSENTIAL] https://gitlab.com/brunhine/Foundry-5e-SpellSchoolIcons
- [NOT ESSENTIAL] https://github.com/ardittristan/VTTColorSettings
- [NOT ESSENTIAL] https://github.com/Brownie79/fvtt-cardimporter
- [NOT ESSENTIAL] https://github.com/schultzcole/FVTT-Backgroundless-Pins
- [NOT ESSENTIAL] https://github.com/earlSt1/vtt-compendium-hider
- [NOT ESSENTIAL] https://gitlab.com/brunhine/foundry-SheetToToken

- [MAP][NOT ESSENTIAL] Point of Interest Character Sheet - https://github.com/Sky-Captain-13/foundry/tree/master/poi-sheet - https://raw.githubusercontent.com/Sky-Captain-13/foundry/master/poi-sheet/poi-sheet/module.json
- [MAP][NEED UPDATE] Critical Fumble - https://github.com/JacobMcAuley/critical-fumble
- [CHAT][NOT ESSENTIAL] Chat Message Accessibility Indicators - https://github.com/schultzcole/VTT-Chat-Message-Accessibility-Indicators - https://github.com/schultzcole/FVTT-Chat-Message-Accessibility-Indicators
- [NOT ESSENTIAL] Max Crit Damage - https://github.com/smilligan93/max-crit-foundry-dnd5e/ - 
- [NOT ESSENTIAL] https://github.com/Forien/foundryvtt-forien-copy-environment
- [NOT ESSENTIAL] Vance Sidebar Resizer - https://github.com/VanceCole/vance-sidebar-resizer
- [NOT ESSENTIAL] Fantasy UI - https://github.com/iotech-fvtt/fantasy-ui - https://raw.githubusercontent.com/iotech-fvtt/fantasy-ui/master/module.json
- [NOT ESSENTIAL] https://github.com/Handyfon/handyfonsminorimprovements/
- [NOT ESSENTIAL] SVG Loader - https://gitlab.com/moerills-fvtt-modules/svg-loader
- [NOT ESSENTIAL] Better Filepicker - https://github.com/Adriannom/fvtt-better-filepicker
- [NOT ESSENTIAL] Squeaker - https://gitlab.com/mesfoliesludiques/foundryvtt-squeaker
- [NOT ESSENTIAL] https://github.com/Handyfon/roll-of-fate
- [NOT ESSENTIAL] https://foundryvtt.com/packages/token-tooltip/
- [NOT ESSENTIAL] https://foundryvtt.com/packages/CautiousGamemastersPack/
- [NOT ESSENTIAL] https://github.com/Sky-Captain-13/foundry/tree/master/powercards
- [NOT ESSENTIAL] https://github.com/syl3r86/image-previewer
- [NOT ESSENTIAL] https://github.com/jennis0/foundryvtt-utils
- [NOT ESSENTIAL] https://github.com/MisterHims/FoundryVTT/tree/master/ScriptMacros/SoundBox/EN
- [NOT ESSENTIAL] https://github.com/syl3r86/favtab
- [NOT ESSENTIAL] https://github.com/schultzcole/FVTT-Default-Image-Overrider
- [NOT ESSENTIAL] Savaged Foundry - https://gitlab.com/tenuki.igo/savaged-foundry
- [NOT ESSENTIAL] BubbleRolls - https://gitlab.com/mesfoliesludiques/foundryvtt-bubblerolls
- [NOT ESSENTIAL] https://github.com/Sk1mble/TimedEvent
- [NOT ESSENTIAL] https://github.com/Sky-Captain-13/foundry/tree/master/expander
- [NOT ESSENTIAL] https://foundryvtt.com/packages/simple-name-generator/
- [NOT ESSENTIAL] SpeakerStats - https://gitlab.com/jvir/foundryvtt-speakerstats
- [NOT ESSENTIAL] https://github.com/ernieayala/ernies-modern-layout

- [DEPRECATED USE https://github.com/Moerill/fvtt-pointer][MAP/TOKEN][LookAtThat](https://github.com/wsaunders1014/LookAtThat/) - https://raw.githubusercontent.com/wsaunders1014/LookAtThat/master/module.json
- [UTILITY][DEPRECATED USE https://github.com/Moerill/fvtt-pointer][Pointer](https://gitlab.com/moerills-fvtt-modules/pointer ) - https://gitlab.com/moerills-fvtt-modules/pointer/raw/master/pointer/module.json

## MACROS Projects

- [MACRO/COMBAT][VanceCole Macros](https://github.com/VanceCole/macros )
- [MACRO][Trigger Happy Teleportation](https://forums.forge-vtt.com/t/trigger-happy-teleportation-tutorial/2117 )
- [MACRO][Trigger Happy Traps](https://forums.forge-vtt.com/t/trigger-happy-traps/3448 )
- [MACRO][Trigger Happy (The Basics)](https://youtu.be/okTWYO4oEMA )

## MODULES FOR MANAGE AUDIO

- [AUDIO][Music d20 Sampler](https://foundryvtt.com/packages/music-d20-sampler/ )
- [AUDIO][Foundry VTT - Game Audio Bundle 1 of 4](https://github.com/datdamnzotz/FoundryVTT-Game-Audio-Bundle-1 ) - https://raw.githubusercontent.com/datdamnzotz/FoundryVTT-Game-Audio-Bundle-1/master/module.json
- [AUDIO][Foundry VTT - Game Audio Bundle 2 of 4](https://github.com/datdamnzotz/FoundryVTT-Game-Audio-Bundle-2 ) - https://raw.githubusercontent.com/datdamnzotz/FoundryVTT-Game-Audio-Bundle-2/master/module.json
- [AUDIO][Foundry VTT - Game Audio Bundle 3 of 4](https://github.com/datdamnzotz/FoundryVTT-Game-Audio-Bundle-3 ) - https://raw.githubusercontent.com/datdamnzotz/FoundryVTT-Game-Audio-Bundle-3/master/module.json
- [AUDIO][Foundry VTT - Game Audio Bundle 4 of 4](https://github.com/datdamnzotz/FoundryVTT-Game-Audio-Bundle-4 ) - https://raw.githubusercontent.com/datdamnzotz/FoundryVTT-Game-Audio-Bundle-4/master/module.json
- [AUDIO][DnDj](https://github.com/horville-hootenkline/DnDj )
- [AUDIO][Music Assist](https://github.com/temportalflux/MusicAssist )
- [AUDIO][Playlist Import](https://github.com/JacobMcAuley/playlist_import ) - https://raw.githubusercontent.com/JacobMcAuley/playlist_import/master/module.json
- [AUDIO][Adventure Music](https://foundryvtt.com/packages/adventuremusic/ )
- [AUDIO][Tabletop RPG Music](https://foundryvtt.com/packages/tabletop-rpg-music/ )

## MODULES MOBILE SUPPORT

- [STYLE][Screenscene](https://github.com/AJAnderson/screenscene )
- [STYLE][Simple Mobile](https://github.com/Handyfon/simplemobile ) - https://raw.githubusercontent.com/Handyfon/simplemobile/master/module.json

## MODULES CHARACTER SHEET

- [CHARACTER SHEET][D&D 5e OGL Character Sheet](https://github.com/ElfFriend-DnD/foundryvtt-5eOGLCharacterSheet ) - https://raw.githubusercontent.com/ElfFriend-DnD/foundryvtt-5eOGLCharacterSheet/master/src/module.json
- [CHARACTER SHEET][Tidy Sheet 5e](https://github.com/sdenec/tidy5e-sheet ) - https://raw.githubusercontent.com/sdenec/tidy5e-sheet/master/module.json
- [CHARACTER SHEET][DNDBeyond Character Sheet for 5E](https://github.com/jopeek/fvtt-dndbeyond-character-sheet ) - https://raw.githubusercontent.com/jopeek/fvtt-dndbeyond-character-sheet/master/module.json
- [CHARACTER SHEET][DND Beyond NPC Sheet](https://github.com/jopeek/fvtt-dndbeyond-npc-sheet ) - https://raw.githubusercontent.com/jopeek/fvtt-dndbeyond-npc-sheet/master/module.json
- [CHARACTER SHEET][Elf Friend 5e Character Sheet](https://github.com/ElfFriend-DnD/foundryvtt-elfFriendCharacterSheet5e ) - https://raw.githubusercontent.com/ElfFriend-DnD/foundryvtt-elfFriendCharacterSheet5e/master/src/module.json
- [CHARACTHER SHEET/UTILITY][External Actor Viewer](https://github.com/ardittristan/VTTExternalActorViewer )

- [CHARACTER SHEET][IS BETTER Tidy Sheet][Illandril's Character Sheet Lockdown (5e)](https://github.com/illandril/FoundryVTT-sheet5e-lockdown )~
- [CHARACTER SHEET][Me5e](https://github.com/sparkcity/fvtt-me5e )

- ~[CHARACTER SHEET][IS BETTER Tidy Sheet][Better NPC Sheet](https://github.com/syl3r86/BetterNPCSheet5e )~
- ~[CHARACTER SHEET][IS BETTER Tidy Sheet][Sky's Alternate 5e Sheet]()~
- ~[CHARACTER SHEET][IS BETTER Tidy Sheet][Dnd5 Dark](https://github.com/stryxin/dnd5edark-foundryvtt )~
- ~[CHARACTER SHEET][IS BETTER Tidy Sheet][Alt5e](https://github.com/Sky-Captain-13/foundry/tree/master/alt5e )~
- ~[CHARACTER SHEET][IS BETTER Tidy Sheet][DNDBeyond Character Sheet for 5E](https://github.com/jopeek/fvtt-dndbeyond-character-sheet/ ) - https://raw.githubusercontent.com/jopeek/fvtt-dndbeyond-character-sheet/master/module.json~
- [CHARACTER SHEET][Darker Dungeons 5e (Darksheet)](https://github.com/Handyfon/Darksheet ) - https://raw.githubusercontent.com/Handyfon/Darksheet/master/module.json

- [CHARACTER SHEET][NEED UPDATE IS BETTER https://github.com/jopeek/fvtt-loot-sheet-npc-5e][Loot Chest NPC](https://gitlab.com/hooking/foundry-vtt---loot-sheet-npc ) - https://gitlab.com/hooking/foundry-vtt---loot-sheet-npc/raw/master/module.json


## MODULES WebRTC/Discord

- [WebRTC Tweaks](https://github.com/bekriebel/fvtt-module-webrtc_tweaks ) - https://raw.githubusercontent.com/bekriebel/fvtt-module-webrtc_tweaks/master/module.json
- [SYSTEM][Discord Bridge](https://github.com/nearlyNonexistent/foundry-discordbridge ) - https://raw.githubusercontent.com/nearlyNonexistent/foundry-discordbridge/master/module.json
- [SYSTEM][Discord Rich Presence](https://github.com/cswendrowski/FoundryVTT-Discord-Rich-Presence ) - https://raw.githubusercontent.com/cswendrowski/FoundryVTT-Discord-Rich-Presence/master/module.json

## MODULES LANGUAGE

- [LANGUAGE][Italian Core](https://gitlab.com/riccisi/foundryvtt-lang-it-it ) - 
- 

## [SYSTEM] PATHFINDER MODULES

- https://github.com/Lo-create/foundry-pf1-content
- https://gitlab.com/unindel/foundry-vtt-pf2e-monster-import-ui/-/raw/master/module.json
- [CHARACTER SHEET][Loot Sheet NPC Pathfinder1](https://github.com/SvenWerlen/fvtt-loot-sheet-npc-pf1 ) - https://raw.githubusercontent.com/SvenWerlen/fvtt-loot-sheet-npc-pf1/master/module.json

## [SYSTEM] MODULES SHADOW RUN 

- [COMPENDIUM][Shadowrun 5e Compendiums](https://foundryvtt.com/packages/shadowrun5e/ ) - https://raw.githubusercontent.com/smilligan93/SR5-Compendiums-FoundryVTT/master/module.json

## [SYSTEM] MODULES STAR WARS 

- [SYSTEM][STAR WARS FFG](https://github.com/jaxxa/StarWarsFFG)

## [SYSTEM] MODULES WARHAMMER

- [SYSTEM][FoundryVTT - Forien's Armoury](https://github.com/Forien/foundryvtt-forien-armoury) - https://raw.githubusercontent.com/Forien/foundryvtt-forien-armoury/master/module.json
- [MAP][WFRP4E - Fan-made Maps for Enemy in Shadows](https://gitlab.com/LeRatierBretonnien/wfrp4e-eis-maps) - https://gitlab.com/LeRatierBretonnien/wfrp4e-eis-maps/-/raw/master/module.json 
- [COMBAT][ENTITY](https://github.com/Foundry-Workshop/ammo-swapper ) - https://raw.githubusercontent.com/Foundry-Workshop/ammo-swapper/master/src/module.json

## [SYSTEM] MODULES OTHER SYSTEMS

- [SYSTEM] [Ratas en las paredes](https://foundryvtt.com/packages/ratasenlasparedes/ )
- [SYSTEM] [kamigakarija](https://github.com/BrotherSharper/kamigakarija )
- [SYSTEM] [13th Age MODULES](https://gitlab.com/asacolips-projects/foundry-mods/archmage )

## DEVELOPING

- [DEV][FoundryGet](https://github.com/cswendrowski/foundryget ) - https://raw.githubusercontent.com/cswendrowski/foundryget/master/module.json
- [DEV][lib-find-the-path](https://github.com/dwonderley/lib-find-the-path/ )
- [DEV][Find the culprit](https://github.com/Moerill/fvtt-find-the-culprit ) - https://raw.githubusercontent.com/Moerill/fvtt-find-the-culprit/master/module.json
- [DEV]Develop Module - https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/tree/master
- [DEV]Create Foundry Project -  https://gitlab.com/foundry-projects/foundry-pc/create-foundry-project
- [DEV]Game Settings - https://github.com/sdenec/fvtt-uii_game-settings
- [DEV]2d image  animated  - https://polycount.com/discussion/186360/animated-2d-portraits-gif-heavy
- [DEV]Ping Times - https://gitlab.com/tenuki.igo/foundryvtt-ping-times
- [DEV]Support library - https://gitlab.com/foundry-projects/foundry-pc/foundry-pc-types
- [DEV]Developing a server - https://theelous3.net/how_to_set_up_foundryvtt_server 

## DISCORD LINK TO EXTERNAL DOCUMENTATION

- [DEV]https://discordapp.com/channels/170995199584108546/699750150674972743/713429213536976988
- [DEV]https://www.reddit.com/r/FoundryVTT/comments/fvw3c7/how_to_create_a_tiny_module_for_shared_content/
- [DEV]https://github.com/foundry-vtt-community/macros/blob/9ee96f173990ecd03cfd924f06d30d317ba78d47/token/token_vision_config.js
- [DEV]https://gist.github.com/janssen-io/50c1990ce7cf31da90fae7e7fb45cf53
- [DEV]Chartopia Extractor - https://github.com/foundry-vtt-community/tables/pull/5/files
- [DEV]Shared Data Module - https://www.reddit.com/r/FoundryVTT/comments/fvw3c7/how_to_create_a_tiny_module_for_shared_content/
- [DEV]Paralixxia Module https://www.reddit.com/r/FoundryVTT/comments/hr0xaq/parallaxia_module_tutorial/fy3zhsv/
- [DEV][Bryans-Preferred-Modules-for-FoundryVTT](https://github.com/bryancasler/Bryans-Preferred-Modules-for-FoundryVTT/edit/master/README.md )

## DEMO SERVER

- [DEMO] Active Demo Servers
- [DEMO] Player Demo: https://foundryvtt.com/demo - player password is foundry
- [DEMO] GM Demo (dnd5e): https://dnd5e.doomnaught.com/ - GM password is foundry
- [DEMO] GM Demo (worldbuilding): https://sws.doomnaught.com/ - GM password is foundry





